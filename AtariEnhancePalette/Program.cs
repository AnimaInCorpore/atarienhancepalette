/*

./AtariEnhancePalette.exe original.png palette.png

*/

using System;
using System.IO;
using System.Drawing;

namespace AtariEnhancePalette
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Bitmap tSourceBitmap = new Bitmap(args[0]);

			int tMaxDelta = 64;

			Color[] tSourcePalette = new Color[16];

			int tSourceColorCounter = 0;

			for(int tPixelY = 0; tPixelY < tSourceBitmap.Height; tPixelY++)
			{
				for(int tPixelX = 0; tPixelX < tSourceBitmap.Width; tPixelX++)
				{
					int tIndex;

					Color tPixel = tSourceBitmap.GetPixel(tPixelX, tPixelY);

					for(tIndex = 0; tIndex < tSourcePalette.Length; tIndex++)
					{
						if(tSourcePalette[tIndex].IsEmpty)
						{
							tSourcePalette[tIndex] = tPixel;
							tSourceColorCounter++;

							break;
						}
						else if(tSourcePalette[tIndex] == tPixel)
						{
							break;
						}
					}
				}
			}

			Array.Sort(tSourcePalette, delegate(Color tColor1, Color tColor2){return (int )((tColor1.R * 0.21 + tColor1.G * 0.71 + tColor1.B * 0.07) - (tColor2.R * 0.21 + tColor2.G * 0.71 + tColor2.B * 0.07));});

			// Create enhanced palette.

			Bitmap tPaletteBitmap = new Bitmap(256, 3, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			for(int tIndex = 0; tIndex < tPaletteBitmap.Width; tIndex++)
			{
				tPaletteBitmap.SetPixel(tIndex, 0, tSourcePalette[0]);
				tPaletteBitmap.SetPixel(tIndex, 1, tSourcePalette[0]);
				tPaletteBitmap.SetPixel(tIndex, 2, tSourcePalette[0]);
			}

			int tEnhancedColorIndex = 0;

			for(int tIndex1 = 0; tIndex1 < tSourceColorCounter; tIndex1++)
			{
				for(int tIndex2 = tIndex1; tIndex2 < tSourceColorCounter; tIndex2++)
				{
					tPaletteBitmap.SetPixel(tEnhancedColorIndex, 0, tSourcePalette[tIndex1]);
					tPaletteBitmap.SetPixel(tEnhancedColorIndex, 1, tSourcePalette[tIndex2]);

					double tR1 = tSourcePalette[tIndex1].R;
					double tG1 = tSourcePalette[tIndex1].G;
					double tB1 = tSourcePalette[tIndex1].B;

					double tR2 = tSourcePalette[tIndex2].R;
					double tG2 = tSourcePalette[tIndex2].G;
					double tB2 = tSourcePalette[tIndex2].B;

					double tGray1 = tR1 * 0.21 + tG1 * 0.71 + tB1 * 0.07;
					double tGray2 = tR2 * 0.21 + tG2 * 0.71 + tB2 * 0.07;

					if(Math.Abs(tGray1 - tGray2) <= tMaxDelta)
						tPaletteBitmap.SetPixel(tEnhancedColorIndex, 2, Color.FromArgb((int )((tR1 + tR2) / 2 + 0.5), (int )((tG1 + tG2) / 2 + 0.5), (int )((tB1 + tB2) / 2 + 0.5)));

					tEnhancedColorIndex++;
				}
			}

			tPaletteBitmap.Save(args[1]);
		}
	}
}
